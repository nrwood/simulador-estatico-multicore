package py.una.pol.simulador.models;

import java.util.List;

public class EstablisedRoute {
//    private GraphPath path;
    private int fsIndexBegin;
    private int fs;
    private int from;
    private int to;
    private List<Link> path;
    private int core;

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public EstablisedRoute() {
    }

    public EstablisedRoute(List<Link> path, int fsIndexBegin, int fs, int from, int to, int core) {
        this.path = path;
        this.fsIndexBegin = fsIndexBegin;
        this.fs = fs;
        this.from = from;
        this.to = to;
        this.core = core;
    }

    public int getCore() {
        return core;
    }

    public void setCore(int core) {
        this.core = core;
    }

    public List<Link> getPath() {
        return path;
    }

    public void setPath(List<Link> path) {
        this.path = path;
    }

    public int getFsIndexBegin() {
        return fsIndexBegin;
    }

    public void setFsIndexBegin(int fsIndexBegin) {
        this.fsIndexBegin = fsIndexBegin;
    }

    public int getFs() {
        return fs;
    }

    public void setFs(int fs) {
        this.fs = fs;
    }

    @Override
    public String toString() {
        return "EstablisedRoute{" +
                "path=" + path +
                ", fsIndexBegin=" + fsIndexBegin +
                ", fs=" + fs +
                ", from=" + from +
                ", to=" + to +
                ", core=" + core +
                '}';
    }



}
