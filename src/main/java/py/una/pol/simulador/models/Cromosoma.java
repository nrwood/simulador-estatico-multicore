/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.simulador.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import org.jgrapht.GraphPath;
import py.una.pol.simulador.utils.Utilitarios;

/**
 *
 * @author Fabi
 */
//un cromosoma representa un individuo de la población que para nosotros es una solución al problema
public class Cromosoma {

    ArrayList<Integer> listaGenes;
    HashMap<Integer, Integer> genesMap;//solo para controlar que los genes creados no sean repetidos
    Boolean seLogroMejora;
    Integer nroReconf;
    double mejora;
    GraphMatrix grafoMejor;
    ArrayList<Integer> indexOrden = new ArrayList<>();
    ArrayList<EstablisedRoute> resultadosActualElegidas = new ArrayList<>();
    ArrayList<EstablisedRoute> resultadosMejor = new ArrayList<>(); //arrayList que guarda el mejor conjunto de resultados
    ArrayList<GraphPath<Integer, Link>> rutasEstablecidasMejor = new ArrayList<>(); //arrayList que guarda el mejor conjunto de resultado
    ArrayList<Integer> indicesMejor = new ArrayList<>(); //arrayList que 
    int cantidadRutasMejor = 0;

    public Cromosoma() {
        listaGenes = new ArrayList<>();
        genesMap = new HashMap<>();//solo para controlar que los genes creados no sean repetidos 
        nroReconf = 0;
        seLogroMejora = Boolean.FALSE;
    }

    public Cromosoma solucionesCandidatas(List<GraphPath<Integer, Link>> rutasEstablecidas,
            double[][][] topologia, GraphMatrix G, int capacidadFSporEnlace,
            ArrayList<EstablisedRoute> resultados, List<GraphPath<Integer, Link>> listaKSP,
            String algoritmoAejecutar, String objetivoAG, int porcentajeLong, int core) {

        boolean porBfr = false, porMsi = false;
        double bfrGrafo = 0, msiGrafo = 0;
        double mejoraActual = 0;
        int cont = 0;
        EstablisedRoute rparcial;
        GraphMatrix copiaGrafo = new GraphMatrix(G.getCantidadDeVertices());
        copiaGrafo.insertarDatos(topologia, 1, 320);
        ArrayList<Integer> indexOrder = new ArrayList<>();
        Cromosoma cr = new Cromosoma();
        int cantReruteosIguales; //para sumar la cantidad de reruteos que quedaron en con los mismos caminos (enlaces y FS)
        ArrayList<GraphPath<Integer, Link>> rutasEstablecidasElegidas;   //guarda las rutasEstablecidas elegidas por un cromosoma
        ArrayList<Integer> indicesElegidas = new ArrayList<>(); //guarda los indices de las rutasEstablecidas elegidas en el vector rutasEstablecidas

        for (int i = 0; i < rutasEstablecidas.size(); i++) {
            indexOrder.add(i);
        }
        //se calcula la longitud del cromosoma
        int longCromosoma = Math.round((rutasEstablecidas.size() * porcentajeLong) / 100);
        //Selecciona el objetivo del algoritmo AG
        switch (objetivoAG) {
            /*case "BFR":
            porBfr = true;
            bfrGrafo = Metricas.BFR(G, capacidadFSporEnlace);
            break;*/
            case "MSI":
                porBfr = false;
                porMsi = true;
                //TODO: Verificar funcionamiento
                msiGrafo = Metricas.MSI(G, capacidadFSporEnlace, core);
                break;
        }

        rutasEstablecidasElegidas = new ArrayList<>();
        indicesElegidas.clear();

        for (int i = 0; i < longCromosoma; i++) {
            indicesElegidas.add(indexOrder.get(elegirRutaAG(indicesElegidas, rutasEstablecidas.size())));
            rutasEstablecidasElegidas.add(rutasEstablecidas.get(indicesElegidas.get(i)));
        }

        while (mejoraActual < mejora && cont < rutasEstablecidas.size()) {
            cantReruteosIguales = 0;
            //Crear la copia del grafo original manualmente
            Utilitarios.copiarGrafo(copiaGrafo, G, capacidadFSporEnlace);
            Utilitarios.desasignarFS_DefragProAct(rutasEstablecidasElegidas, resultados, copiaGrafo, indicesElegidas, core); //desasignamos los FS de las rutasEstablecidas a reconfigurar                
            //ORDENAR LISTA
            if (rutasEstablecidasElegidas.size() > 1) {
                ordenarRutas(resultados, rutasEstablecidasElegidas, indicesElegidas, rutasEstablecidasElegidas.size());
            }
            //volver a rutear con las nuevas condiciones mismo algoritmo
            int contBloqueos = 0;
            for (int i = 0; i < rutasEstablecidasElegidas.size(); i++) {
                int fs = resultados.get(indicesElegidas.get(i)).getTo() - resultados.get(indicesElegidas.get(i)).getFrom();
                fs++;
                //int tVida = G.acceder(rutasEstablecidas.get(indicesElegidas.get(i)).getInicio().getDato(),rutasEstablecidas.get(indicesElegidas.get(i)).getInicio().getSiguiente().getDato()).getFS()[resultados.get(indicesElegidas.get(i)).getInicio()].getTiempo();
                Demand demandaActual = new Demand(rutasEstablecidasElegidas.get(i).getStartVertex(), rutasEstablecidasElegidas.get(i).getEndVertex(), fs);
                GraphPath<Integer, Link> ksp = listaKSP.get(indicesElegidas.get(i));
                rparcial = Utilitarios.realizarRuteo(algoritmoAejecutar, demandaActual, copiaGrafo, listaKSP, capacidadFSporEnlace, demandaActual.getCore());
                if (rparcial != null) {
                    Utilitarios.asignarFS_Defrag(ksp, rparcial, copiaGrafo, demandaActual, 0);
                    //verificar si eligio el mismo camino y fs para no sumar en reruteadas
                    if (compararRutas(rparcial, resultados.get(indicesElegidas.get(i)))) {
                        cantReruteosIguales++;
                    }
                } else {
                    contBloqueos++;
                }
            }
            //si hubo bloqueo no debe contar como una solucion
            if (contBloqueos == 0) {
                mejoraActual = Utilitarios.calculoMejoraAG(porMsi, porBfr, copiaGrafo, bfrGrafo, capacidadFSporEnlace, msiGrafo, core);
            } else {
                mejoraActual = 0;
            }
            if (mejoraActual > 0) {
                for (int i = 0; i < indicesElegidas.size(); i++) {
                    listaGenes.add(indicesElegidas.get(i));
                }
                cr.setListaGenes(listaGenes);
                cr.setNroReconf(listaGenes.size() - cantReruteosIguales);
                cr.setMejora(mejoraActual);
                cr.setSeLogroMejora(Boolean.FALSE);
            } else {
                listaGenes.clear();
                genesMap.clear();
                cr.genesMap.clear();
                cr.listaGenes.clear();
            }
        }
        return cr;
    }

    //ver si dos resultados son iguales
    public static Boolean compararRutas(EstablisedRoute r, EstablisedRoute r2) {
        return r.getFrom() == r2.getFrom() && r.getTo() == r2.getTo();
    }

    /**
     * Metodo que ordena las rutasEstablecidas elegidas por las hormigas para su
     * posterior re-ruteo por orden decreciente de cantidad de FS requeridos
     * @param resultados
     * @param rutasEstablecidas
     * @param indices
     * @param rutasEstablecidasElegidasSize
     */
    public static void ordenarRutas(List<EstablisedRoute> resultados, List<GraphPath<Integer, Link>> rutasEstablecidas, List<Integer> indices, int rutasEstablecidasElegidasSize) {
        Integer aux;
        GraphPath<Integer, Link> aux2;
        for (int i = 0; i <= rutasEstablecidasElegidasSize - 1; i++) {
            for (int j = i + 1; j < rutasEstablecidasElegidasSize; j++) {
                Integer fin = resultados.get(indices.get(j)).getTo() - resultados.get(indices.get(j)).getFrom();
                Integer inicio = resultados.get(indices.get(i)).getTo() - resultados.get(indices.get(i)).getFrom();
                if (fin > inicio) {
                    //cambia el orden del array de indices
                    aux = indices.get(i);
                    indices.set(i, indices.get(j));
                    indices.set(j, aux);

                    //cambia el orden en el array de rutasEstablecidas
                    aux2 = rutasEstablecidas.get(i);
                    rutasEstablecidas.set(i, rutasEstablecidas.get(j));
                    rutasEstablecidas.set(j, aux2);
                }
            }
        }
    }

    public HashMap<Integer, Integer> getGenesMap() {
        return genesMap;
    }

    public void setGenesMap(HashMap<Integer, Integer> genesMap) {
        this.genesMap = genesMap;
    }

    public Boolean getSeLogroMejora() {
        return seLogroMejora;
    }

    public void setSeLogroMejora(Boolean seLogroMejora) {
        this.seLogroMejora = seLogroMejora;
    }

    public ArrayList<Integer> getListaGenes() {
        return listaGenes;
    }

    public void setListaGenes(ArrayList<Integer> listaGenes) {
        this.listaGenes = listaGenes;
    }

    public Integer getNroReconf() {
        return nroReconf;
    }

    public void setNroReconf(Integer nroReconf) {
        this.nroReconf = nroReconf;
    }

    public double getMejora() {
        return mejora;
    }

    public void setMejora(double mejora) {
        this.mejora = mejora;
    }

    public GraphMatrix getGrafoMejor() {
        return grafoMejor;
    }

    public void setGrafoMejor(GraphMatrix grafoMejor) {
        this.grafoMejor = grafoMejor;
    }

    public ArrayList<Integer> getIndexOrden() {
        return indexOrden;
    }

    public void setIndexOrden(ArrayList<Integer> indexOrden) {
        this.indexOrden = indexOrden;
    }

    public ArrayList<Integer> getIndicesMejor() {
        return indicesMejor;
    }

    public void setIndicesMejor(ArrayList<Integer> indicesMejor) {
        this.indicesMejor = indicesMejor;
    }

    public int getCantidadRutasMejor() {
        return cantidadRutasMejor;
    }

    public void setCantidadRutasMejor(int cantidadRutasMejor) {
        this.cantidadRutasMejor = cantidadRutasMejor;
    }

    public Cromosoma buenasSoluciones(List<GraphPath<Integer, Link>> rutasEstablecidas, double[][][] topologia, GraphMatrix G,
            int capacidadFSporEnlace, ArrayList<EstablisedRoute> resultados, ArrayList<GraphPath<Integer, Link>> listaKSP, String algoritmoAejecutar,
            int porcentajeLongCRAG, String objetivoAG, int cantidadBuenasSol, ArrayList<Integer> mejorSol, int cont, int core) {
        boolean porBfr = false, porMsi = false;
        double bfrGrafo = 0, msiGrafo = 0;
        double mejoraActual;
        EstablisedRoute rparcial;
        GraphMatrix copiaGrafo = new GraphMatrix(G.getCantidadDeVertices());
        copiaGrafo.insertarDatos(topologia, 1, 320);
        Cromosoma cr = new Cromosoma();
        int cantReruteosIguales; //para sumar la cantidad de reruteos que quedaron en con los mismos caminos (enlaces y FS)
        ArrayList<GraphPath<Integer, Link>> rutasEstablecidasElegidas = new ArrayList<>();  //guarda las rutasEstablecidas elegidas para un cromosoma
        ArrayList<Integer> indicesElegidas = new ArrayList<>(); //guarda los indices de las rutasEstablecidas elegidas en el vector rutasEstablecidas
        ArrayList<Integer> solucion = new ArrayList<>();

        //se calcula la longitud del cromosoma
        int longCromosoma = Math.round((rutasEstablecidas.size() * porcentajeLongCRAG) / 100);
        //Selecciona el objetivo del AG
        switch (objetivoAG) {
            case "MSI":
                porBfr = false;
                porMsi = true;
                msiGrafo = Metricas.MSI(G, capacidadFSporEnlace, core);
                break;
        }
        //Crear una buena solucion,dependiendo de la metrica estudiada
        switch (objetivoAG) {
            case "MSI":
                solucion = crearSolucionMSI(resultados, rutasEstablecidas, longCromosoma, cantidadBuenasSol, mejorSol, cont, core);
                break;
        }
        //Evaluar la solucion propuesta
        rutasEstablecidasElegidas.clear();
        indicesElegidas.clear();
        mejoraActual = 0;
        for (int i = 0; i < longCromosoma; i++) {
            indicesElegidas.add(solucion.get(i));
            rutasEstablecidasElegidas.add(rutasEstablecidas.get(indicesElegidas.get(i)));
        }
        cantReruteosIguales = 0;
        //Crear la copia del grafo original manualmente
        Utilitarios.copiarGrafo(copiaGrafo, G, capacidadFSporEnlace);
        Utilitarios.desasignarFS_DefragProAct(rutasEstablecidasElegidas, resultados, copiaGrafo, indicesElegidas, core); //desasignamos los FS de las rutasEstablecidas a reconfigurar                
        //Ordena las rutasEstablecidas por requerimiento de FS
        if (rutasEstablecidasElegidas.size() > 1) {
            ordenarRutas(resultados, rutasEstablecidasElegidas, indicesElegidas, rutasEstablecidasElegidas.size());
        }
        //volver a rutear con las nuevas condiciones mismo algoritmo
        int contBloqueos = 0;
        for (int i = 0; i < rutasEstablecidasElegidas.size(); i++) {
            int fs = resultados.get(indicesElegidas.get(i)).getTo() - resultados.get(indicesElegidas.get(i)).getFrom();
            fs++;
            //int tVida = G.acceder(rutasEstablecidas.get(indicesElegidas.get(i)).getInicio().getDato(),rutasEstablecidas.get(indicesElegidas.get(i)).getInicio().getSiguiente().getDato()).getFS()[resultados.get(indicesElegidas.get(i)).getInicio()].getTiempo();
            Demand demandaActual = new Demand(rutasEstablecidasElegidas.get(i).getStartVertex(), rutasEstablecidasElegidas.get(i).getEndVertex(), fs);
            GraphPath<Integer, Link> ksp = listaKSP.get(indicesElegidas.get(i));
            rparcial = Utilitarios.realizarRuteo(algoritmoAejecutar, demandaActual, copiaGrafo, listaKSP, capacidadFSporEnlace, core);
            if (rparcial != null) {
                Utilitarios.asignarFS_Defrag(ksp, rparcial, copiaGrafo, demandaActual, 0);
                //verificar si eligio el mismo camino y fs para no sumar en reruteadas
                if (compararRutas(rparcial, resultados.get(indicesElegidas.get(i)))) {
                    cantReruteosIguales++;
                }
            } else {
                contBloqueos++;
            }
        }
        //si hubo bloqueo no debe contar como una solucion
        if (contBloqueos == 0) {
            mejoraActual = Utilitarios.calculoMejoraAG(porMsi, porBfr, copiaGrafo, bfrGrafo, capacidadFSporEnlace, msiGrafo, core);
        } else {
            mejoraActual = 0;
        }
        if (mejoraActual > 0) {
            for (int i = 0; i < indicesElegidas.size(); i++) {
                listaGenes.add(indicesElegidas.get(i));
            }
            cr.setListaGenes(listaGenes);
            cr.setNroReconf(listaGenes.size() - cantReruteosIguales);
            cr.setMejora(mejoraActual);
            cr.setSeLogroMejora(Boolean.FALSE);
        } else {//si no es solucion retorna un solucion vacia
            listaGenes.clear();
            cr.listaGenes.clear();
        }
        return cr;
    }

    //crear una solucion teniendo en cuenta el msi
    public static ArrayList<Integer> crearSolucionMSI(List<EstablisedRoute> resultados,
            List<GraphPath<Integer, Link>> rutasEstablecidas, int longCromosoma, int cantSoluciones,
            List<Integer> mejorSolucion, int cont, int core) {
        double[] valorMSIrutasEstablecidas = new double[rutasEstablecidas.size()];
        ArrayList<Integer> indexOrden = new ArrayList<>();
        // ArrayList<Integer> solucion = new ArrayList<>();
        ArrayList<Integer> mejorSolucionAux = new ArrayList<>();
        ArrayList<GraphPath<Integer, Link>> rutasEstablecidasElegidas = new ArrayList<>();
        ArrayList<Integer> indicesElegidas = new ArrayList<>();
        int rutasEstablecidasQueParticipan = 0;
        int aux = -1;
        double sumatoria;
        double[] probabilidad = new double[rutasEstablecidas.size()];
        for (int i = 0; i < mejorSolucion.size(); i++) {
            mejorSolucionAux.add(mejorSolucion.get(i));
        }
        if (cont == 0) {
            for (int i = 0; i < rutasEstablecidas.size(); i++) {
                valorMSIrutasEstablecidas[i] = resultados.get(i).getTo();//Obtengo los msi de las rutasEstablecidas  
            }
            for (int i = 0; i < valorMSIrutasEstablecidas.length; i++) {
                indexOrden.add(i);
            }
            //ordenar vector valorMSIrutasEstablecidas de acuerdo a su probabilidad
            ordenarRutasMsi(valorMSIrutasEstablecidas, indexOrden);
            int pos = valorMSIrutasEstablecidas.length - 1;
            while (indicesElegidas.size() < longCromosoma) {
                indicesElegidas.add(indexOrden.get(pos));
                pos = pos - 1;
            }
        } else {
            for (int i = 0; i < rutasEstablecidas.size(); i++) {
                valorMSIrutasEstablecidas[i] = resultados.get(i).getTo();//Obtengo los msi de las rutasEstablecidas  
            }
            for (int i = 0; i < valorMSIrutasEstablecidas.length; i++) {
                indexOrden.add(i);
            }
            //calcular la probabilidad
            sumatoria = 0.0;
            for (int i = 0; i < rutasEstablecidas.size(); i++) {
                sumatoria = sumatoria + valorMSIrutasEstablecidas[i];
            }
            //for(int i=rutasEstablecidasQueParticipan+1;i<rutasEstablecidas.size();i++){
            for (int i = 0; i < rutasEstablecidas.size(); i++) {
                probabilidad[i] = (valorMSIrutasEstablecidas[i]) / sumatoria;
            }
            //ordenar vector indice de acuerdo a su probabilidad
            Utilitarios.ordenarProbabilidad(probabilidad, indexOrden);
            for (int i = 0; i < longCromosoma; i++) {
                indicesElegidas.add(indexOrden.get(Utilitarios.elegirRuta(probabilidad, indicesElegidas, indexOrden)));
            }
        }
        return indicesElegidas;
    }

    public static void ordenarRutasMsi(double[] listaRutasMsi, ArrayList<Integer> orden) {
        double auxp;
        int auxi;
        int n = listaRutasMsi.length;
        for (int i = 0; i <= n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (listaRutasMsi[i] > listaRutasMsi[j]) {
                    auxp = listaRutasMsi[i];
                    listaRutasMsi[i] = listaRutasMsi[j];
                    listaRutasMsi[j] = auxp;

                    //cambiar el orden del vector de indices
                    auxi = orden.get(i);
                    orden.set(i, orden.get(j));
                    orden.set(j, auxi);
                }
            }
        }
    }
    //Ordenar las rutasEstablecidas por BFR

    public static void ordenarRutasBFR(double[] listaRutasBFR, ArrayList<Integer> orden) {
        double auxp;
        int auxi;
        int n = listaRutasBFR.length;
        for (int i = 0; i <= n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (listaRutasBFR[i] > listaRutasBFR[j]) {
                    auxp = listaRutasBFR[i];
                    listaRutasBFR[i] = listaRutasBFR[j];
                    listaRutasBFR[j] = auxp;
                    //cambiar el orden del vector de indices
                    auxi = orden.get(i);
                    orden.set(i, orden.get(j));
                    orden.set(j, auxi);
                }
            }
        }
    }

    public static int elegirRutaAG(ArrayList<Integer> indices, int rutasEstablecidasActivas) {
        Random randomGenerator = new Random();
        int indiceAleatorio = randomGenerator.nextInt(rutasEstablecidasActivas);
        while (Utilitarios.isInList(indices, indiceAleatorio)) {
            indiceAleatorio = randomGenerator.nextInt(rutasEstablecidasActivas);
        }
        return indiceAleatorio;
    }

}
