package py.una.pol.simulador.models;

import py.una.pol.simulador.utils.Utilitarios;
import java.util.ArrayList;

/**
 *
 * @author Team Delvalle El grafo en su forma de matriz de adyacencia para
 * representar la topologia de una red. Almacena: Los datos de los enlaces para
 * cada vertice i,j La cantidad de vertices La capacidad total que es la catidad
 * de FSs disponibles por enlace en la red. El ancho de cada FS.
 */
public class GraphMatrix {

    private final Link[][] vertices;
    private Boolean[] marks;
    private Integer vertexQuantity;
    private Integer totalCapacity;
    private Double widthFS;
    private Integer core;

    public GraphMatrix(Integer V) {

        this.marks = new Boolean[V];
        this.vertices = new Link[V][V];
        this.vertexQuantity = V;
        this.totalCapacity = 0;
        this.widthFS = new Double(0);

    }

    public GraphMatrix() {

        this.vertexQuantity = 0;
        this.marks = null;
        this.vertices = null;
    }

    public int getCore() {
        return core;
    }

    public void setCore(int core) {
        this.core = core;
    }

    public GraphMatrix insertarDatos(double[][][] v, int cores, int fs) {
        double nro;

        for (int i = 0; i < this.vertices.length; i++) {
            for (int j = 0; j < this.vertices.length; j++) {
                if (v[i][j][1] != 0) {
                    nro = v[i][j][0];
                    this.vertices[i][(int) nro] = new Link(i, (int) nro, cores, fs);
                    this.vertices[(int) nro][i] = new Link((int) nro, i, cores, fs);
                }
            }
        }
        return this;
    }

    public void marcar(int i) {
        marks[i] = true;
    }

    public boolean getMarca(int i) {
        return marks[i];
    }

    public Link acceder(int i, int j) {
        System.out.println("Desde " + i + " hasta " + j);
         return this.vertices[i][j];
    }

    public int getCantidadDeVertices() {
        return this.vertexQuantity;
    }

    public int getCapacidadTotal() {
        return this.totalCapacity;
    }

    public double getAnchoFS() {
        return this.widthFS;
    }

    public int getCantidadEnlaces() {
        int cont = 0;
        ArrayList<Integer> lista = new ArrayList<>();
        for (int i = 0; i < this.vertexQuantity; i++) {
            for (int j = 0; j < this.vertexQuantity; j++) {
                if (this.acceder(i, j) != null) {
                    if (!Utilitarios.isInList(lista, j)) {
                        cont++;
                    }
                }
            }
            lista.add(i);
        }
        return cont;
    }

    /*public double entropia() {

        double uelink = 0;
        double entropy = 0;
        int countlinks = 0;
        for (int i = 0; i < this.getCantidadDeVertices(); i++) {
            for (int j = 0; j < this.getCantidadDeVertices(); j++) {
                int UEcont = 0;
                if (this.acceder(i, j) != null) {
                    for (int kk = 0; kk < this.acceder(i, j).getFS().length - 1; kk++) {
                        //System.out.println("fs tamano: "+ this.acceder(i, j).getFS().length);
                        //System.out.println("fs tamano: "+ this.acceder(i, j).getFS()[kk]);
                        //System.out.println("fs-utili--> "+ this.acceder(i, j).getFS()[kk].getEstado() +"::"+ this.acceder(i, j).getUtilizacion()[kk]);
                        if (this.acceder(i, j).getFS()[kk].getEstado() != this.acceder(i, j).getFS()[kk + 1].getEstado()) {
                            UEcont++;
                        }
                    }
                    uelink = uelink + (double) UEcont;//(this.acceder(i, j).getFS().length-1));
                    countlinks++;
                    //System.in.read();
                }
            }
        }
        //System.out.println("cantidad de links::::: "+ countlinks);
        entropy = uelink / countlinks;
        return entropy;
    }*/

    /*public void ResetRed() {
        for (int ii = 0; ii < this.getCantidadDeVertices(); ii++) {
            for (int jj = 0; jj < this.getCantidadDeVertices(); jj++) {
                if (this.acceder(ii, jj) != null) {
                    for (int ci = 0; ci < this.acceder(ii, jj).getFS().length; ci++) {
                        this.acceder(ii, jj).getFS()[ci].setEstado(1);
                        //System.out.println(G[8].acceder(ii, jj).getVertice1()+"......");
                    }
                }
            }
        }
    }*/
}
