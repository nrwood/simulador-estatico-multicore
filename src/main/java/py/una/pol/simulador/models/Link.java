package py.una.pol.simulador.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Link implements Serializable {
    private int distance;
    private List <Core> cores;
    private int from;
    private int to;
    private int fsWidth;
    private int fs;

    public Link(int distance, List<Core> cores, int from, int to) {
        this.distance = distance;
        this.cores = cores;
        this.from = from;
        this.to = to;
    }
    
    
    public Link(int v1,int v2, int cores, int fs){
        this.from=v1;
        this.to=v2;
        this.cores = new ArrayList<>();
        for(int i = 0; i<cores; i++) {
            this.cores.add(new Core(320, fs));
        }
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public List<Core> getCores() {
        return cores;
    }

    public void setCores(List<Core> cores) {
        this.cores = cores;
    }

    public int getFsWidth() {
        return fsWidth;
    }

    public void setFsWidth(int fsWidth) {
        this.fsWidth = fsWidth;
    }

    public int getFs() {
        return fs;
    }

    public void setFs(int fs) {
        this.fs = fs;
    }

    @Override
    public String toString() {
        return "Link{" +
//                "distance=" + distance +
//                ", cores=" + cores.size() +
//                ", from=" + from +
//                ", to=" + to +
                + from + " - " + to +
                '}';
    }
    
    public boolean equals(Link link) {
        return link.from == from && link.to == to;
    }


}
