/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.una.pol.simulador.models;

/**
 *
 * @author Néstor E. Reinoso Wood
 */
public enum TopologiasEnum {
    NSFNET(14, "nsfnet"),
    USNET(24, "usnet"),
    EUNET(15, "eunet"),
    NETWORKS(6, "networks"),
    SMALLNET(3, "smallnet");
    private TopologiasEnum(Integer size, String file) {
        this.size = size;
        this.file = file;
    }
    
    private Integer size;
    
    private String file;
    
    public Integer size() {
        return size;
    }
    
    public String file() {
        return file;
    }
    
    public static TopologiasEnum fromFile(String fileName) {
        for(TopologiasEnum value : TopologiasEnum.values()) {
            if(value.file.equals(fileName)) {
                return value;
            }
        }
        return null;
    }
}
