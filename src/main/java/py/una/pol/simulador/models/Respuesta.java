/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.una.pol.simulador.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Néstor E. Reinoso Wood
 */
public class Respuesta implements Serializable {

    public Respuesta() {
        this.entropy = new ArrayList<>();
        this.msi = new ArrayList<>();
        this.bfr = new ArrayList<>();
        this.lightPathQuantity = new ArrayList<>();
        this.consecutivePaths = new ArrayList<>();
        this.entropyOverUse = new ArrayList<>();
        
    }
    private List<List<BigDecimal>> entropy;
    private List<List<BigDecimal>> msi;
    private List<List<BigDecimal>> bfr;
    private List<List<BigDecimal>>lightPathQuantity;
    private List<List<BigDecimal>> consecutivePaths;
    private List<List<BigDecimal>>entropyOverUse;
    public Integer desfragmentaciones;
    public Integer fallidas;
    public Integer demandas;
    public Integer bloqueos;

    public List<List<BigDecimal>> getEntropy() {
        return entropy;
    }

    public void setEntropy(List<List<BigDecimal>> entropy) {
        this.entropy = entropy;
    }

    public List<List<BigDecimal>> getMsi() {
        return msi;
    }

    public void setMsi(List<List<BigDecimal>> msi) {
        this.msi = msi;
    }

    public List<List<BigDecimal>> getBfr() {
        return bfr;
    }

    public void setBfr(List<List<BigDecimal>> bfr) {
        this.bfr = bfr;
    }

    public List<List<BigDecimal>> getLightPathQuantity() {
        return lightPathQuantity;
    }

    public void setLightPathQuantity(List<List<BigDecimal>> lightPathQuantity) {
        this.lightPathQuantity = lightPathQuantity;
    }

    public List<List<BigDecimal>> getConsecutivePaths() {
        return consecutivePaths;
    }

    public void setConsecutivePaths(List<List<BigDecimal>> consecutivePaths) {
        this.consecutivePaths = consecutivePaths;
    }

    public List<List<BigDecimal>> getEntropyOverUse() {
        return entropyOverUse;
    }

    public void setEntropyOverUse(List<List<BigDecimal>> entropyOverUse) {
        this.entropyOverUse = entropyOverUse;
    }

    public Integer getDesfragmentaciones() {
        return desfragmentaciones;
    }

    public void setDesfragmentaciones(Integer desfragmentaciones) {
        this.desfragmentaciones = desfragmentaciones;
    }

    public Integer getFallidas() {
        return fallidas;
    }

    public void setFallidas(Integer fallidas) {
        this.fallidas = fallidas;
    }

    public Integer getDemandas() {
        return demandas;
    }

    public void setDemandas(Integer demandas) {
        this.demandas = demandas;
    }

    public Integer getBloqueos() {
        return bloqueos;
    }

    public void setBloqueos(Integer bloqueos) {
        this.bloqueos = bloqueos;
    }
}