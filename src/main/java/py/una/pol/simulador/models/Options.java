package py.una.pol.simulador.models;

/**
 * Datos de entrada para la simulación
 *
 * @author Néstor E. Reinoso Wood
 */
public class Options {

    private Boolean desfragmentar;
    private int capacity;
    private int demandsQuantity;
    private int fsRangeMin;
    private int fsRangeMax;
    private int cores;
    private int porcentageDesfrag;
    private float fsWidth;
    private String metricaDesfrag;
    private String topology;
    private String routingAlg;
    private String preroutingAlg;
    private String demandsOrder;
    
    // Algoritmo AG
    private String objetivoMejora;
    private Integer cantIndividuos;
    private Integer cantGeneraciones;
    private Integer porcentajeMejora;

    public Options() {
    }

    public int getDemandsQuantity() {
        return demandsQuantity;
    }

    public void setDemandsQuantity(int demandsQuantity) {
        this.demandsQuantity = demandsQuantity;
    }

    public int getFsRangeMin() {
        return fsRangeMin;
    }

    public void setFsRangeMin(int fsRangeMin) {
        this.fsRangeMin = fsRangeMin;
    }

    public int getFsRangeMax() {
        return fsRangeMax;
    }

    public void setFsRangeMax(int fsRangeMax) {
        this.fsRangeMax = fsRangeMax;
    }

    public int getCores() {
        return cores;
    }

    public void setCores(int cores) {
        this.cores = cores;
    }

    public String getTopology() {
        return topology;
    }

    public void setTopology(String topology) {
        this.topology = topology;
    }

    public String getRoutingAlg() {
        return routingAlg;
    }

    public void setRoutingAlg(String routingAlg) {
        this.routingAlg = routingAlg;
    }

    public Boolean getDesfragmentar() {
        return desfragmentar;
    }

    public void setDesfragmentar(Boolean desfragmentar) {
        this.desfragmentar = desfragmentar;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getPorcentageDesfrag() {
        return porcentageDesfrag;
    }

    public void setPorcentageDesfrag(int porcentageDesfrag) {
        this.porcentageDesfrag = porcentageDesfrag;
    }

    public float getFsWidth() {
        return fsWidth;
    }

    public void setFsWidth(float fsWidth) {
        this.fsWidth = fsWidth;
    }

    public String getMetricaDesfrag() {
        return metricaDesfrag;
    }

    public void setMetricaDesfrag(String metricaDesfrag) {
        this.metricaDesfrag = metricaDesfrag;
    }

    public String getPreroutingAlg() {
        return preroutingAlg;
    }

    public void setPreroutingAlg(String preroutingAlg) {
        this.preroutingAlg = preroutingAlg;
    }

    public String getDemandsOrder() {
        return demandsOrder;
    }

    public void setDemandsOrder(String demandsOrder) {
        this.demandsOrder = demandsOrder;
    }

    public String getObjetivoMejora() {
        return objetivoMejora;
    }

    public void setObjetivoMejora(String objetivoMejora) {
        this.objetivoMejora = objetivoMejora;
    }

    public Integer getCantIndividuos() {
        return cantIndividuos;
    }

    public void setCantIndividuos(Integer cantIndividuos) {
        this.cantIndividuos = cantIndividuos;
    }

    public Integer getCantGeneraciones() {
        return cantGeneraciones;
    }

    public void setCantGeneraciones(Integer cantGeneraciones) {
        this.cantGeneraciones = cantGeneraciones;
    }

    public Integer getPorcentajeMejora() {
        return porcentajeMejora;
    }

    public void setPorcentajeMejora(Integer porcentajeMejora) {
        this.porcentajeMejora = porcentajeMejora;
    }
}
