/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.simulador.algorithms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import py.una.pol.simulador.models.GraphMatrix;
import py.una.pol.simulador.models.Demand;
import py.una.pol.simulador.utils.Utils;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.graph.GraphWalk;
import py.una.pol.simulador.models.Core;
import py.una.pol.simulador.models.EstablisedRoute;
import py.una.pol.simulador.models.FrecuencySlot;
import py.una.pol.simulador.models.Link;

/**
 *
 * @author sFernandez
 */
public class Algoritmos_Defrag_ProAct {

    /*   public static EstablisedRoute Def_FACA(GraphMatrix G, Demand demand,List<GraphPath<Integer, Link>> kspaths, int capacity){  // LAS ENTRADAS DEBEMOS REDEFINIR SEGUN LAS CLASES UTILIZADAS
        
        //*Definicion de variables las variables
        int count; // posicion inicial y final dentro del espectro asi como el countador de FSs countiguos disponibles
        int sgteBloque;//bandera para avisar que tiene que ir al siguiente bloque
        
//        int demandColocada=0; // bandera para countrolar si ya se encountro espectro disponible para la demand.
        boolean so[]= new boolean [capacity]; //Ocupacion de Espectro.
        List<GraphPath<Integer, Link>>kspPlaced = new ArrayList<>();
        ArrayList<Integer> inicios = new ArrayList<>();
        ArrayList<Integer> fines = new ArrayList<>();
        ArrayList<Integer> indiceKsp = new ArrayList<>();
        ArrayList<Integer> indKSPUbicMenFcmt = new ArrayList<>();

        //Probamos para cada camino, si existe espectro para ubicar la damanda
        int k=0;

        while(k<kspaths.length && kspaths[k]!=null){
            //Inicializadomos el espectro, inicialmente todos los FSs estan libres
            Arrays.fill(so, false);//Se inicializa todo el espectro como libre

            //Calcular la ocupacion del espectro para cada camino k
            for(int i=0;i<capacity;i++){
                for(Nodo n=kspaths[k].getInicio();n.getSiguiente().getSiguiente()!=null;n=n.getSiguiente()){
                   //System.out.println("v1 "+n.getDato()+" v2 "+n.getSiguiente().getDato()+" cant vertices "+G.getCantidadDeVertices()+" i "+i+" FSs "+G.acceder(n.getDato(),n.getSiguiente().getDato()).getFS().length);
                    if(G.acceder(n.getDato(),n.getSiguiente().getDato()).getFS()[i].getEstado()==0){
                        so[i]=true;
                        break;
                    }
                }
            }
            
            //encuentra las posibles asignaciones
            count=0;
            sgteBloque = 0; 
            for(int i=0;i<capacity;i++){
                if(so[i]=false && sgteBloque == 0){
                    count++;
                }else if (so[i]=true){
                    count=0;
                    sgteBloque = 0;
                }
                //si se encountro un bloque valido, tomamos en cuenta el kspaths
                if(count==demand.getNroFS()){
                    fines.add(i);
                    inicios.add(i - count + 1);
                    kspPlaced.add(kspaths[k]);
                    indiceKsp.add(k);
                    sgteBloque = 1;
                    count = 0;
//                    break; //solo agrega el primero que encuentra
                }
            }
            k++;
        }
        
        if (kspPlaced.isEmpty()){ //bloqueo
            //System.out.println("Desubicado");
            return null;
        }
        
        //cuenta los cortes para cada posible asiganción
//        int cutsSlot; //cantidad de cortes
        int ind = 0; //aux indice del kspathsUbicado actual
        int corte = 999; //cant de cortes de la opcion del kspaths
        EstablisedRoute r = new EstablisedRoute();
        double FcmtAux = -1;
        double Fcmt = 9999999;
        int caminoElegido = -1;
        
        //por cada índice de los posibles caminos de cada KSP ubicado
        for (ListaEnlazada kspathsUbi : kspPlaced){
            corte = 0;
            for (Nodo n = kspathsUbi.getInicio(); n.getSiguiente().getSiguiente() != null; n = n.getSiguiente()) {
                if (inicios.get(ind) != 0 && fines.get(ind) < capacity - 1) { //para que no tome los bordes sup e inf
                    if (G.acceder(n.getDato(), n.getSiguiente().getDato()).getFS()[inicios.get(ind) - 1].getEstado() == 1
                            && G.acceder(n.getDato(), n.getSiguiente().getDato()).getFS()[fines.get(ind) + 1].getEstado() == 1) {
                        corte = corte + 1;
                    }
                }
            }
            
            //calcula el desalineamiento de cada uno
            int Desalineacion = 0;
            int bandEsRuta = 0;
            for (Nodo n = kspathsUbi.getInicio(); n.getSiguiente().getSiguiente() != null; n = n.getSiguiente()) {
                for (int i = 0; i < G.getCantidadDeVertices(); i++) {
                    if(G.acceder(n.getDato(), i)!=null){ //si son vecinos
                        bandEsRuta = 0;
                        for (Nodo nn = kspathsUbi.getInicio(); nn.getSiguiente().getSiguiente() != null; nn = nn.getSiguiente()) {
                            if(nn.getDato() == i){ //si es parte de la ruta
                                bandEsRuta = 1;
                            }
                        }
                        if(bandEsRuta == 0){
                            for(int fsd = inicios.get(ind); fsd <= inicios.get(ind) + demand.getNroFS() - 1; fsd++){
                                if (G.acceder(i, n.getDato()).getFS()[fsd].getEstado() == 1) {
                                    Desalineacion = Desalineacion + 1;
                                } else {
                                    Desalineacion = Desalineacion - 1;
                                }
                            }
                        }
                    }
                }
            }
            
            //calcular la capacity actual
            double capacityLibre = (double)Utilitarios.countarCapacidadLibre(kspathsUbi,G,capacity);
        
            double saltos = (double)Utilitarios.calcularSaltos(kspathsUbi);
            double vecinos = (double)Utilitarios.countarVecinos(kspathsUbi,G,capacity);
            
            FcmtAux = corte + (Desalineacion/(demand.getNroFS()*vecinos)) + (saltos *(demand.getNroFS()/capacityLibre)); 
            
            if (FcmtAux<Fcmt){
                Fcmt = FcmtAux;
                indKSPUbicMenFcmt.clear();
                indKSPUbicMenFcmt.add(ind);
            }else if(FcmtAux==Fcmt){
                indKSPUbicMenFcmt.add(ind);
            }
        
            ind++;
        }
        
        if (indKSPUbicMenFcmt.size() == 1){
            r.setPath(indiceKsp.get(indKSPUbicMenFcmt.get(0)));
            r.setTo(fines.get(indKSPUbicMenFcmt.get(0)));
            r.setFrom(inicios.get(indKSPUbicMenFcmt.get(0)));
        }else{
            //encountrar el kspaths más corto
            int tamKspMasCorto = 999;
            int indKspMasCorto = -1;
            for (int indMenCutsMenDesalig : indKSPUbicMenFcmt) {
                if (kspPlaced.get(indMenCutsMenDesalig).getTamanho() - 1 < tamKspMasCorto){
                    indKspMasCorto = indMenCutsMenDesalig;
                    tamKspMasCorto = kspPlaced.get(indMenCutsMenDesalig).getTamanho() - 1;
                }
            }
            //buscar el indice first fit
            //Inicializadomos el espectro, inicialmente todos los FSs estan libres
            for(int i=0;i<capacity;i++){
                so[i]=false;
            }
            //Calcular la ocupacion del espectro para cada camino k
            for(int i=0;i<capacity;i++){
                for(Nodo n=kspaths[indiceKsp.get(indKspMasCorto)].getInicio();n.getSiguiente().getSiguiente()!=null;n=n.getSiguiente()){
                    if(G.acceder(n.getDato(),n.getSiguiente().getDato()).getFS()[i].getEstado()==0){
                        so[i]=true;
                        break;
                    }
                }
            }
            count=0; 
            for(int i=0;i<capacity;i++){
                if(so[i]=false){
                    count++;
                }else if (so[i]=true){
                    count=0;
                }
                //si se encountro un bloque valido, tomamos en cuenta el kspaths
                if(count==demand.getNroFS()){ //si o si va a encountrar ya que es un kspaths en kspPlaced
                    r.setPath(indiceKsp.get(indKspMasCorto));
                    r.setTo(i);
                    r.setFrom(i - count + 1);
                    break; //solo el primero que encuentra
                }
            }
        }
        

        return r;
    }*/
    public static EstablisedRoute Def_FA(GraphMatrix G, Demand demand, List<GraphPath<Integer, Link>> kspaths, int capacity, int core) {  // REDEFINIR LAS ENTRADAS

        //*Definicion de variables las variables
        int count, j;; // posicion inicial y final dentro del espectro asi como el countador de FSs countiguos disponibles
        int sgteBloque;//bandera para avisar que tiene que ir al siguiente bloque

//        int demandColocada=0; // bandera para countrolar si ya se encountro espectro disponible para la demanda.
        boolean so[] = new boolean[capacity]; //Ocupacion de Espectro.
        List<GraphPath<Integer, Link>> kspPlaced = new ArrayList<>();
        ArrayList<Integer> inicios = new ArrayList<>();
        ArrayList<Integer> fines = new ArrayList<>();
        ArrayList<Integer> indiceKsp = new ArrayList<>();

        //Probamos para cada camino, si existe espectro para ubicar la damanda
        int k = 0;
        while (k < kspaths.size() && kspaths.get(k) != null) {
            //Inicializadomos el espectro, inicialmente todos los FSs estan libres
            Arrays.fill(so, false);//Se inicializa todo el espectro como libre
            GraphPath ksp = kspaths.get(k);
            // para cada Frequency slot en el enlace
            for (int i = 0; i < capacity; i++) {
                // Por cada vértice en el camino
                for (Object path : ksp.getEdgeList()) {
                    // Verifica que el Frequency slot esté libre
                    Link link = (Link) path;
                    FrecuencySlot fs = link.getCores().get(core).getFs().get(i);
                    if (!fs.isFree()) {
                        so[i] = true;
                        break;
                    }
                }
            }

            //encuentra las posibles asignaciones
            capacity:
            for (int i = 0; i < capacity; i++) {
                count = 0;
                if (!so[i]) {
                    for (j = i; j < capacity; j++) {
                        if (!so[j]) {
                            count++;
                        } else {
                            i = j;
                            break;
                        }
                        //si se encountro un bloque valido, tomamos en cuenta el kspaths
                        //Mantenemos los vectores fines, inicios e indices para posteriormente usar en la comparación de cuts 
                        if (count == demand.getFs()) {
                            fines.add(i);
                            inicios.add(i - count + 1);
                            kspPlaced.add(kspaths.get(k));
                            indiceKsp.add(k);
                            count = 0;
                            break capacity; //solo agrega el primero que encuentra   (inicialmente en fabiana esto estaba comentado, en enciso no ojo)
                        }
                    }
                    if (j == capacity) {
                        break;
                    }
                }
            }
            k++;
        }

        if (kspPlaced.isEmpty()) { //bloqueo
            return null;
        }

        //cuenta los cortes para cada posible asiganción
//              int cutsSlot; //cantidad de cortes
        int ind = 0; //aux indice del kspathsUbicado actual
        int cutAux = 0; //cant de cortes del camino kspaths
        int cuts = 999; //el menor corte, 999 como referencia inicial
        EstablisedRoute establisedRoute = new EstablisedRoute();
        int DesalineacionFinal = 999;

        //vectores con los menores cortes
        ArrayList<Integer> indKSPUbicMenCuts = new ArrayList<>();
        ArrayList<Integer> indKSPUbicMenCutsMenDesalig = new ArrayList<>();

        //por cada índice de los posibles caminos de cada KSP ubicado
        for (GraphPath<Integer, Link> kspathsUbi : kspPlaced) {
            //for (Nodo n = kspathsUbi.getStartVertex(); n.getSiguiente().getSiguiente() != null; n = n.getSiguiente()) {
            for (Object path : kspathsUbi.getEdgeList()) {
                Link link = (Link) path;
// POR FAVOR REVISAR LÓGICA
                if (inicios.get(ind) != 0 && fines.get(ind) < capacity - 1) { //para que no tome los bordes sup e inf
                    FrecuencySlot fs1 = link.getCores().get(core).getFs().get(inicios.get(ind) - 1);
                    FrecuencySlot fs2 = link.getCores().get(core).getFs().get(fines.get(ind) + 1);
                    if (fs1.isFree() && fs2.isFree()) {
                        cutAux++;
                    }
                }
            }
            //encuentra el/los menor/es
            if (cutAux < cuts) {
                //si hay un menor al menor, limpia el vector y solo deja ese
                cuts = cutAux;
                indKSPUbicMenCuts.clear();
                indKSPUbicMenCuts.add(ind);
            } else if (cutAux == cuts) {
                indKSPUbicMenCuts.add(ind);
            }
            cutAux = 0;
            ind++;
        }

        //si hay un solo menor cut entonces es elegido, sino se calcula el alineamiento de los menores
        if (indKSPUbicMenCuts.size()
                == 1) {
            GraphPath kspPath = kspaths.get(indKSPUbicMenCuts.get(0));
            establisedRoute.setPath(kspPath.getEdgeList());
            establisedRoute.setTo(fines.get(indKSPUbicMenCuts.get(0)));
            establisedRoute.setFrom(inicios.get(indKSPUbicMenCuts.get(0)));
        } else {
            //calcula el desalineamiento de cada uno
            for (int indMenorCuts : indKSPUbicMenCuts) {

                //calcula el desalineamiento de cada uno
                int Desalineacion = 0;
                boolean bandEsRuta = false;
                //AQUI TENEMOS QUE VER COMO ES EL INDICE CON MENOR CUTS
                // for (Nodo n = kspPlaced.get(indMenorCuts).getInicio(); n.getSiguiente().getSiguiente() != null; n = n.getSiguiente()) {
                GraphPath kspMenorCuts = kspaths.get(indMenorCuts);
                for (Object path : kspMenorCuts.getEdgeList()) {
                    Link link = (Link) path;
                    for (int i = 0; i < kspMenorCuts.getLength(); i++) {    // Este lenght viene a ser cantidad de vertices?
// ESTARÁ CORRECTO?  
                        if (G.acceder(link.getFrom(), i) != null) {     // Link puede equiparse con Nodo? 
                            //if (G.acceder(n.getDato(), i) != null) { //si son vecinos
                            bandEsRuta = false;
                            for (Object path2 : kspMenorCuts.getEdgeList()) {
                                Link link2 = (Link) path2;
                                if (link2.getFrom() == i) { //si es parte de la ruta
                                    bandEsRuta = true;
                                    break;
                                }
                            }
                            if (bandEsRuta == false) {
                                for (int fsd = inicios.get(indMenorCuts); fsd <= inicios.get(indMenorCuts) + demand.getFs() - 1; fsd++) {
                                    FrecuencySlot fs = G.acceder(i, link.getFrom()).getCores().get(core).getFs().get(fsd);
                                    if (fs.isFree() == false) {
                                        Desalineacion++;
                                    } else {
                                        Desalineacion--;
                                    }
                                }
                            }
                        }
                    }
                }

                if (Desalineacion < DesalineacionFinal) {
                    //si hay un menor al menor, limpia el vector y solo deja ese
                    DesalineacionFinal = Desalineacion;
                    indKSPUbicMenCutsMenDesalig.clear();
                    indKSPUbicMenCutsMenDesalig.add(indMenorCuts);
                } else if (Desalineacion == DesalineacionFinal) {
                    indKSPUbicMenCutsMenDesalig.add(indMenorCuts);
                }
            }

            //si hay un solo menor cut con menor desalineación entonces es elegido, sino envie al shorter KSP y hace first Fit
            if (indKSPUbicMenCutsMenDesalig.size() == 1) {
                GraphPath kspPath = kspaths.get(indKSPUbicMenCutsMenDesalig.get(0));
                establisedRoute.setPath(kspPath.getEdgeList());
                establisedRoute.setTo(fines.get(indKSPUbicMenCutsMenDesalig.get(0)));
                establisedRoute.setFrom(inicios.get(indKSPUbicMenCutsMenDesalig.get(0)));
            } else { //calcula el shorter KSP y hace first Fit  
                //encountrar el kspaths más corto
                int tamKspMasCorto = 999;
                int indKspMasCorto = -1;
                for (int indMenCutsMenDesalig : indKSPUbicMenCutsMenDesalig) {
// AQUI USAMOS EL GET WEIGHT O LENGHT?
                    if (kspPlaced.get(indMenCutsMenDesalig).getLength() - 1 < tamKspMasCorto) {
                        indKspMasCorto = indMenCutsMenDesalig;
                        tamKspMasCorto = kspPlaced.get(indMenCutsMenDesalig).getLength() - 1;
                    }
                }
                //buscar el indice first fit
                //Inicializadomos el espectro, inicialmente todos los FSs estan libres
                for (int i = 0; i < capacity; i++) {
                    so[i] = false;
                }
                //Calcular la ocupacion del espectro para cada camino k
                for (int i = 0; i < capacity; i++) { // ACA TOMA EL MAS CORTO
                    GraphPath kspMasCorto = kspaths.get(indKspMasCorto);
                    for (Object path : kspMasCorto.getEdgeList()) {
                        Link link = (Link) path;
                        //for (Nodo n = ksp[indiceKsp.get(indKspMasCorto)].getInicio(); n.getSiguiente().getSiguiente() != null; n = n.getSiguiente()) {
                        //if (G.acceder(n.getDato(), n.getSiguiente().getDato()).getFS()[i].getEstado() == 0) {
                        Link accedido = G.acceder(link.getFrom(), link.getTo());
                        
                        // TODO: Solución temporal para que no explote
                        if (accedido != null) {
                            FrecuencySlot fs = accedido.getCores().get(core).getFs().get(i);
                            if (fs.isFree() == true) {
                                so[i] = true;
                                break;
                            }
                        }
                    }
                }
                count = 0;
                for (int i = 0; i < capacity; i++) {
                    if (so[i] = false) {
                        count++;
                    } else if (so[i] = true) {
                        count = 0;
                    }
                    //si se encountro un bloque valido, tomamos en cuenta el kspaths
                    if (count == demand.getFs()) { //si o si va a encountrar ya que es un kspaths en kspPlaced
                        GraphPath kspPath = kspaths.get(indKspMasCorto);
                        establisedRoute.setPath(kspPath.getEdgeList());
                        establisedRoute.setTo(i);
                        establisedRoute.setFrom(i - count + 1);
                        break; //solo el primero que encuentra
                    }
                }
            }
        }
        return establisedRoute;
    }
}
