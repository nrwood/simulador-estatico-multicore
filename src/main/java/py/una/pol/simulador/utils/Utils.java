package py.una.pol.simulador.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Random;

import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.graph.GraphWalk;
import org.jgrapht.graph.SimpleWeightedGraph;
import py.una.pol.simulador.algorithms.Algorithms;
import py.una.pol.simulador.models.Core;
import py.una.pol.simulador.models.Demand;
import py.una.pol.simulador.models.EstablisedRoute;
import py.una.pol.simulador.models.FrecuencySlot;
import py.una.pol.simulador.models.Link;

public class Utils {

    public static List<Demand> generateDemands(int demandasQuantity, int fsMin, int fsMax, int cantNodos) {
        int j;
        int source;
        int destination;
        int fs;
        List<Demand> demands = new ArrayList<>();
        Random rand;
        for (j = 0; j < demandasQuantity; j++) {
            rand = new Random();
            source = rand.nextInt(cantNodos);
            destination = rand.nextInt(cantNodos);
            fs = (int) (Math.random() * (fsMax - fsMin + 1)) + fsMin;
            while (source == destination) {
                destination = rand.nextInt(cantNodos);
            }
            demands.add(new Demand(source, destination, fs));
        }
        return demands;
    }

    public static List<Demand> readDemands(String topology, Integer quantity) throws Exception {
        List<Demand> result = new ArrayList<>();
        String fileName = String.format("demands_%s_%s.csv", topology, quantity);
        InputStream is = ResourceReader.getFileFromResourceAsStream(fileName);

        BufferedReader br;
        br = new BufferedReader(new InputStreamReader(is));

        // Saltea línea del encabezado del archivo CSV
        br.readLine();
        String linea;
        while ((linea = br.readLine()) != null) {
            Demand demanda = lineToDemand(linea);
            result.add(demanda);
        }
        return result;
    }

    private static Demand lineToDemand(String line) {
        String[] values = line.split(";");
        Demand demand = new Demand(Integer.valueOf(values[0]), Integer.valueOf(values[1]), Integer.valueOf(values[2]));
        return demand;
    }

    public static int poisson(int lambda) {
        int b, bFact;
        double s, a;
        double e = Math.E;
        a = (Math.random() * 1) + 0;
        b = 0;
        bFact = factorial(b);
        s = (Math.pow(e, (-lambda))) * ((Math.pow(lambda, b)) / (bFact));
        while (a > s) {
            b++;
            bFact = factorial(b);
            s = s + ((Math.pow(e, (-lambda))) * ((Math.pow(lambda, b)) / (bFact)));
        }
        return b;
    }

    public static int getTimeLife(int ht) {
        int b;
        double s, a, aux, auxB, auxHT;
        double e = Math.E;
        a = (Math.random() * 1) + 0;
        b = 1;
        auxB = (double) b;
        auxHT = (double) ht;
        aux = (-1) * (auxB / auxHT);
        s = 1 - (Math.pow(e, (aux)));
        while (s < a) {
            b++;
            auxB = (double) b;
            aux = (-1) * (auxB / auxHT);
            s = 1 - (Math.pow(e, (aux)));
        }
        return b;
    }

    public static int factorial(int n) {
        int resultado = 1;
        for (int i = 1; i <= n; i++) {
            resultado *= i;
        }
        return resultado;
    }

    public static Map<String, Integer> countCuts(Graph<Integer, Link> graph, List<GraphPath<Integer, Link>> ksp, int capacity, int core, int fs) {
        Map<String, Integer> slotCuts;
        ArrayList<Map<String, Integer>> bestKspSlot = new ArrayList<>();

        for (int k = 0; k < ksp.size(); k++) {
//            System.out.println("----------------------------------");
//            System.out.println(ksp.get(k));
            slotCuts = numCuts(ksp.get(k), graph, capacity, core, fs);
//            System.out.println("Mejor slot: " + slotCuts.get("slot") + " con " + slotCuts.get("cuts") + " cuts");
            if (bestKspSlot.isEmpty() || slotCuts.get("cuts") < bestKspSlot.get(0).get("cuts")) { //Primera vez o si encuentra encuentra un resultado mejor (menos cuts)
                bestKspSlot.clear();//Limpiamos el array porque pueden haber más de un resultado guardado
                slotCuts.put("ksp", k);//Guardamos el indice del mejor ksp
                bestKspSlot.add(slotCuts);
            } else if (Objects.equals(slotCuts.get("cuts"), bestKspSlot.get(0).get("cuts"))) {//Si tienen igual cantidad de cortes guardamos
                slotCuts.put("ksp", k);
                bestKspSlot.add(slotCuts);
            }
        }
//        if (slotCuts.size() == 1) //Solo un resultado
//            return bestKspSlot.get(0);

        int finalPath;
        finalPath = alignmentCalc(ksp, graph, bestKspSlot, core);
        return bestKspSlot.get(finalPath);
    }

    public static int alignmentCalc(List<GraphPath<Integer, Link>> ksp, Graph<Integer, Link> graph, ArrayList<Map<String, Integer>> kspSlot, int core) {
        int lessMisalign = -1;
        int lessMisalignAux;
        int bestIndex = 0;
        int c = 0;
        for (Map<String, Integer> k : kspSlot) {
            lessMisalignAux = countMisalignment(ksp.get(k.get("ksp")), graph, core, k.get("slot"));
            if (lessMisalign == -1 || lessMisalignAux < lessMisalign) {
                lessMisalign = lessMisalignAux;
//                bestIndex = k.get("ksp");
                bestIndex = c;//Tengo que guardar el indice en kspSlot, no el indice en ksp
            }
            c++;
        }
        return bestIndex;
    }

    public static int countMisalignment(GraphPath<Integer, Link> ksp, Graph<Integer, Link> graph, int core, int slot) {
        int missalign = 0;
        for (Link link : ksp.getEdgeList()) {//Por cada enlace
            for (Link fromNeighbour : graph.outgoingEdgesOf((link).getFrom())) {//Vecinos por el nodo origen
                if (!ksp.getEdgeList().contains(fromNeighbour)) {//Verificamos que el vecino no este en el camino
                    if ((fromNeighbour).getCores().get(core).getFs().get(slot).isFree())//Si el slot elegido esta ocupado ocurre desalineación
                    {
                        missalign++;
                    }
                }
            }
            for (Link toNeighbour : graph.outgoingEdgesOf((link).getTo())) {//Vecinos por el nodo destino
                if (!ksp.getEdgeList().contains(toNeighbour)) {//Verificamos que el vecino no este en el camino
                    if ((toNeighbour).getCores().get(core).getFs().get(slot).isFree())//Si el slot elegido esta ocupado ocurre desalineación
                    {
                        missalign++;
                    }
                }
            }
        }
//        System.out.println("DESALINEACIÓN: " + missalign);
        return missalign;
    }

    public static int countNeighbour(GraphPath<Integer, Link> ksp, Graph<Integer, Link> graph) {
        List<Link> neighbours = new ArrayList<>();
        for (Link link : ksp.getEdgeList()) {
            for (Link fromNeighbour : graph.outgoingEdgesOf((link).getFrom())) {
                if (!ksp.getEdgeList().contains(fromNeighbour) && !neighbours.contains(fromNeighbour)) {
                    neighbours.add(fromNeighbour);
                }
            }
            for (Link toNeighbour : graph.outgoingEdgesOf((link).getTo())) {
                if (!ksp.getEdgeList().contains(toNeighbour) && !neighbours.contains(toNeighbour)) {

                    neighbours.add(toNeighbour);
                }
            }
        }
        System.out.println("Vecinos: " + neighbours);
        return neighbours.size();
    }

    public static int countFreeCapacity(GraphPath ksp, Graph graph, int core, int capacity) {

        int frees = 0;
        for (int i = 0; i < capacity; i++) {
            for (Object path : ksp.getEdgeList()) {
                Link link = (Link) path;
                FrecuencySlot fs = link.getCores().get(core).getFs().get(i);
                if (fs.isFree()) {
                    frees++;
                }
            }
        }

        return frees;
    }

    public static Map<String, Integer> numCuts(GraphPath<Integer, Link> ksp, Graph<Integer, Link> graph, int capacity, int core, int fs) {
        int cuts = -1;
        int slot = -1;
        Map<String, Integer> slotCuts = new HashMap<>();

        List<Integer> cIndexes;
        int cutAux = 0;
        cIndexes = searchIndexes(ksp, graph, capacity, core, fs, true);

        for (int slotIndex : cIndexes) {
            if (slotIndex != 0) {
                for (Object link : ksp.getEdgeList()) {
                    if (((Link) link).getCores().get(core).getFs().get(slotIndex - 1).isFree()) {
                        cutAux++;//Se encontro un lugar vacio en el slot i - 1 del ksp actual
                    }
                }
            }
//            System.out.println("Para el cIndex: " + slotIndex + ", cuts = " + cutAux);
            if (cuts == -1 || cutAux < cuts) {
                cuts = cutAux;
                slot = slotIndex;
            }
            cutAux = 0;
        }

        slotCuts.put("cuts", cuts);
        slotCuts.put("slot", slot);
        return slotCuts;

    }

    public static List<GraphPath<Integer, Link>> twoLinksRoutes(Graph<Integer, Link> g) {
        List<GraphPath<Integer, Link>> paths = new ArrayList<>();
        for (int i = 0; i < g.vertexSet().size(); i++) {
            for (Link link1 : g.outgoingEdgesOf(i)) {
                for (Link link2 : g.outgoingEdgesOf((link1).getTo())) {
                    if (!link1.equals(link2)) {
                        List<Link> path = new ArrayList<>();
                        path.add(link1);
                        path.add(link2);

                        GraphWalk<Integer, Link> gw = new GraphWalk<>(g, i, link2.getTo(), path, 1);
                        paths.add(gw);
                    }

                }
            }
        }

        return paths;
    }

    public static List<Integer> searchIndexes(GraphPath<Integer, Link> ksp, Graph<Integer, Link> graph, int capacity, int core, int fsQ, boolean checkFs) {
        List<Integer> indexes = new ArrayList<>();
        boolean free;
        boolean canBeCandidate = true;//Inicialmente el primer slot puede ser candidato
        int slots = 0;
        for (int i = 0; i < capacity; i++) {
            free = true;
            for (Object path : ksp.getEdgeList()) {
                Link link = (Link) path;
                FrecuencySlot fs = link.getCores().get(core).getFs().get(i);
                if (!fs.isFree()) {//Se verifica que todo el camino este libre en el slot i
                    free = false;
                    canBeCandidate = true;//Cuando encuentra un slot ocupado entonces el siguiente puede ser candidato
                    slots = 0;
                    break;
                }
            }
            if (free)//Si esta libre se aumenta el contador
            {
                slots++;
            }
            if (slots == fsQ && canBeCandidate) {//Si puede contener la cantidad de fs y es candidadto valido entonces se agrega
                indexes.add(i - fsQ + 1);
                slots = 0;
                canBeCandidate = false;
            }
        }
        return indexes;
    }

    public static void assignFs(EstablisedRoute establisedRoute, int core) {
        for (Object link : establisedRoute.getPath()) {
            for (int i = establisedRoute.getFsIndexBegin(); i < establisedRoute.getFsIndexBegin() + establisedRoute.getFs(); i++) {
                ((Link) link).getCores().get(core).getFs().get(i).setFree(false);
            }
        }
    }

    public static void deallocateFs(Graph<Integer, Link> graph, EstablisedRoute establisedRoute) {
        int core = establisedRoute.getCore();
        for (Link link : establisedRoute.getPath()) {
            Link linkAux = graph.getEdge(link.getFrom(), link.getTo());
            for (int i = establisedRoute.getFsIndexBegin(); i < establisedRoute.getFsIndexBegin() + establisedRoute.getFs(); i++) {
                linkAux.getCores().get(core).getFs().get(i).setFree(true);
            }
        }
    }

    public static int getCore(int limit, boolean[] tested) {
        Random r = new Random();
        int core = r.nextInt(limit);
        while (tested[core]) {
            core = r.nextInt(limit);
        }
        tested[core] = true;
        return core;
    }

    public static double graphEntropyCalculation(Graph graph) {
        List<FrecuencySlot> fs;
        double uelink = 0;
        int ueCount = 0;
        int cores = 0;
        for (Object link : graph.edgeSet()) {
            cores = ((Link) link).getCores().size();
            for (int core = 0; core < cores; core++) {
                fs = ((Link) link).getCores().get(core).getFs();
                ueCount = 0;
                for (int i = 0; i < fs.size() - 1; i++) {
                    if (fs.get(i).isFree() != fs.get(i + 1).isFree()) {
                        ueCount++;
                    }
                }
            }
            uelink += ueCount;
        }

        return uelink / graph.edgeSet().size() * cores;
    }
    
    public static int executionPercentage(int execDemands,int totalDemands){
        int percentage;
        percentage = Math.round(100 * execDemands / totalDemands) ;
        return percentage;
    }

    public static Graph<Integer, Link> copyGraph(Graph<Integer, Link> graph) {
        Graph<Integer, Link> copy = new SimpleWeightedGraph<>(Link.class);

        for (int i = 0; i < graph.vertexSet().size(); i++) {
            System.out.println(i);
            copy.addVertex(i);
        }
        List<Link> links = new ArrayList<>();
        links.addAll(graph.edgeSet());
        for (Link link : links) {
            List<Core> cores = new ArrayList<>();
            for (Core core : link.getCores()) {
                List<FrecuencySlot> espectro = new ArrayList<>();

                for (FrecuencySlot fs : core.getFs()) {
                    FrecuencySlot fsCopy = new FrecuencySlot(core.getBandwidth() / core.getFs().size());
                    fsCopy.setFree(fs.isFree());
                    espectro.add(fsCopy);
                }
                Core copyCore = new Core(core.getBandwidth(), espectro);
                cores.add(copyCore);
            }

            Link clink = new Link(link.getDistance(), cores, link.getFrom(), link.getTo());
            copy.addEdge(link.getFrom(), link.getTo(), clink);
            copy.setEdgeWeight(clink, link.getDistance());
        }
        return copy;
    }

    public static boolean compareRoutes(EstablisedRoute r1, EstablisedRoute r2) {
        if (r1.getPath().size() != r2.getPath().size()) {
            return false;
        }
        String rs1, rs2;
        for (int l = 0; l < r1.getPath().size(); l++) {
            rs1 = r1.getPath().get(l).getFrom() + "-" + r1.getPath().get(l).getTo();
            rs2 = r2.getPath().get(l).getFrom() + "-" + r2.getPath().get(l).getTo();
            if (!rs1.equals(rs2)) {
                return false;
            }
        }
        return true;
    }

    @SuppressWarnings("unchecked")
    public static <T> T deepCopy(T obj)
            throws Exception {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(bout);

        out.writeObject(obj);
        out.flush();

        ByteArrayInputStream bin = new ByteArrayInputStream(bout.toByteArray());
        ObjectInputStream in = new ObjectInputStream(bin);

        obj = (T) in.readObject();
        return obj;
    }
    public static void quicksort(List<Demand> A, int izq, int der) {
        int i=izq;         // i realiza la búsqueda de izquierda a derecha
        int j=der;         // j realiza la búsqueda de derecha a izquierda
        Demand pivote= (Demand) A.get(izq); // tomamos primer elemento como pivote
        Demand aux;

        while(i < j){                          // mientras no se crucen las búsquedas                                   
            while(A.get(i).getFs() <= pivote.getFs() && i < j) i++; // busca elemento mayor que pivote
                while(A.get(j).getFs() > pivote.getFs()) j--;           // busca elemento menor que pivote
                if (i < j) {                        // si no se han cruzado                      
                    aux= (Demand) A.get(i);                      // los intercambia
                    A.set(i, A.get(j));
                    A.set(j, aux);
                }
        }

        A.set(izq, A.get(j));      // se coloca el pivote en su lugar de forma que tendremos                                    
        A.set(j, pivote);      // los menores a su izquierda y los mayores a su derecha

        if(izq < j-1)
            quicksort(A,izq,j-1);          // ordenamos subarray izquierdo
        if(j+1 < der)
            quicksort(A,j+1,der);          // ordenamos subarray derecho
    }
}