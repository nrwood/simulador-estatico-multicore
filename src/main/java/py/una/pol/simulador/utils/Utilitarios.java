package py.una.pol.simulador.utils;

import java.awt.Color;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.XYTextAnnotation;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jgrapht.GraphPath;
import py.una.pol.simulador.algorithms.Algoritmos_Defrag_ProAct;
import py.una.pol.simulador.models.Demand;
import py.una.pol.simulador.models.EstablisedRoute;
import py.una.pol.simulador.models.GraphMatrix;
import py.una.pol.simulador.models.Link;
import py.una.pol.simulador.models.Metricas;

/**
 *
 * @author Team Delvalle
 */
public class Utilitarios {

    //<editor-fold defaultstate="collapsed" desc="Utilitarios para tablas">
    public static void reiniciarJTableRows(javax.swing.JTable tabla) {
        if (tabla != null) {
            DefaultTableModel modelo = (DefaultTableModel) tabla.getModel();
            while (modelo.getRowCount() > 0) {
                modelo.removeRow(0);
            }
        }
    }

    public static void reiniciarJTableColumns(javax.swing.JTable tabla) {
        if (tabla != null) {
            DefaultTableModel modelo = (DefaultTableModel) tabla.getModel();
            modelo.setColumnCount(0);
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Demandas">
    public static List<Demand> readDemands(String topology, Integer quantity) throws Exception {
        List<Demand> result = new ArrayList<>();
        String fileName = String.format("demands_%s_%s.csv", topology, quantity);
        InputStream is = ResourceReader.getFileFromResourceAsStream(fileName);

        BufferedReader br;
        br = new BufferedReader(new InputStreamReader(is));

        // Saltea línea del encabezado del archivo CSV
        br.readLine();
        String linea;
        while ((linea = br.readLine()) != null) {
            Demand demanda = lineToDemand(linea);
            result.add(demanda);
        }
        return result;
    }

    private static Demand lineToDemand(String line) {
        String[] values = line.split(";");
        Demand demand = new Demand(Integer.valueOf(values[0]), Integer.valueOf(values[1]), Integer.valueOf(values[2]));
        return demand;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Archivos">
    public static double redondearDecimales(double valorInicial, int numeroDecimales) {
        double resultado = valorInicial * Math.pow(10, numeroDecimales);
        resultado = Math.round(resultado);
        resultado = Math.floor(resultado);
        resultado = resultado / (Math.pow(10, numeroDecimales));

        return resultado;
    }

    public static void escribirArchivoEstados(File archivo, int core, double msi, int establishedroutes, boolean blocked) {
        BufferedWriter bw;
        try {
            if (archivo.exists()) {
                bw = new BufferedWriter(new FileWriter(archivo, true));
            } else {
                bw = new BufferedWriter(new FileWriter(archivo));
                //bw.write("bloqueo,msi,rutas,pathconsec,entropiauso,porcuso");
                bw.write("blocked,core,msi,established routes");
                bw.write("\r\n");
            }
            if (blocked) {
                bw.write("" + 1);
            } else {
                bw.write("" + 0);
            }
            bw.write(",");
            bw.write("" + core);
            bw.write(",");
            bw.write("" + redondearDecimales(msi, 3));
            bw.write(",");
            bw.write("" + establishedroutes);
            bw.write("\r\n");
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return;
    }

    //public static void escribirArchivoResultados(File archivo, int tiempo, int cantB, int cantD, double entropia, double MSI, double BFR, int cantRutas, double pathConsec, double entropiaUso, double porcUso, double probBloqueo){
    public static void escribirArchivoResultados(File archivo, int demanda, int core, int cantB, double MSI, int cantRutas) {
        BufferedWriter bw;
        try {
            if (archivo.exists()) {
                bw = new BufferedWriter(new FileWriter(archivo, true));
            } else {
                bw = new BufferedWriter(new FileWriter(archivo));
            }
            bw.write("" + demanda);
            bw.write(",");
            bw.write("" + core);
            bw.write(",");
            bw.write("" + redondearDecimales(MSI, 3));
            
            /*bw.write(",");
        bw.write("" + redondearDecimales(entropia, 3));*/
            bw.write(",");
            bw.write("" + cantB);
            /*bw.write(",");
        bw.write("" + redondearDecimales(BFR, 3));*/
            bw.write(",");
            bw.write("" + cantRutas);
            /* bw.write(",");
        bw.write("" + redondearDecimales(pathConsec, 3));
        bw.write(",");
        bw.write("" + redondearDecimales(entropiaUso, 3));
        bw.write(",");
        bw.write("" + redondearDecimales(porcUso, 3));
        bw.write(",");
        bw.write("" + redondearDecimales(probBloqueo, 3));*/
            bw.write("\r\n");
            bw.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return;
    }

    public static void escribirArchivoMSI(File archivo, List<List<Double>> cores) throws IOException {
        BufferedWriter bw;
        boolean flag = true;
        int count = -1;
        if (archivo.exists()) {
            bw = new BufferedWriter(new FileWriter(archivo, true));
        } else {
            bw = new BufferedWriter(new FileWriter(archivo));
        }
        while (flag == true) {
            ++count;
            int aux = 0;
            flag = false;
            //IMPRESION DE RESULTADOS (COLUMNAS) EN FILA ACTUAL
            for (int i = 0; i < cores.size(); i++) {
                if (count < cores.get(i).size()) {
                    bw.write("" + redondearDecimales(cores.get(i).get(count), 3));
                    aux++;
                    if (aux < cores.get(i).size() - 1) {
                        bw.write(",");
                    }
                    flag = true;
                }
            }
            //SALTO DE FILA
            bw.write("\r\n");
        }
        bw.close();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Graficador">
    // GRAFICADOR DISEÑADO PARA CORES = 1,2,5,7,9
    public static void GraficarResultado(XYSeries series[], List<XYTextAnnotation> annotation, JScrollPane panelResultados, List<List<Double>> cores) throws FileNotFoundException, IOException {
        XYSeriesCollection datos = new XYSeriesCollection();
        panelResultados.removeAll();
        final XYPlot subplot[] = new XYPlot[cores.size()];
        String axis[] = new String[cores.size()];
        XYItemRenderer renderer[] = new XYItemRenderer[cores.size()];
        NumberAxis rangeAxis[] = new NumberAxis[cores.size()];
        
        for (int i = 0; i < cores.size(); i++) {
            axis[i] = "CORE " + (i+1);
        }

        for (int i = 0; i < cores.size(); i++) {
            // create subplot for all cores...
            datos.addSeries(series[i]);
            renderer[i] = new StandardXYItemRenderer();
            rangeAxis[i] = new NumberAxis(axis[i]);
            rangeAxis[i].setAutoRangeIncludesZero(false);
            subplot[i] = new XYPlot(datos, null, rangeAxis[i], renderer[i]);
            subplot[i].setRangeAxisLocation(AxisLocation.TOP_OR_LEFT);
            datos = new XYSeriesCollection();
        }
        for (XYTextAnnotation anno : annotation) {
            for (int i = 0; i < cores.size(); i++) {
                anno.setFont(new Font("SansSerif", Font.PLAIN, 15));
                //subplot[i].addAnnotation(anno);
                ValueMarker marker = new ValueMarker(anno.getX());          // position is the value on the axis
                marker.setPaint(Color.black);       //marker.setLabel("here"); // see JavaDoc for labels, colors, strokes
                subplot[i].addDomainMarker(marker);
            }
        }

        // parent plot...
        // TITULO DEL EJE X
        final CombinedDomainXYPlot plot = new CombinedDomainXYPlot(new NumberAxis("CANTIDAD DE DEMANDAS"));
        plot.setGap(10);
        // add the subplots...
        for (int i = 0; i < cores.size(); i++) {
            plot.add(subplot[i]);
        }
        plot.setOrientation(PlotOrientation.VERTICAL);

        final JFreeChart chart = new JFreeChart(null, JFreeChart.DEFAULT_TITLE_FONT, plot, true);
        final ChartPanel panel = new ChartPanel(chart, true, true, true, false, true);
        //panel.setPreferredSize(new java.awt.Dimension(500, 270));
        //setContentPane(panel);
        panel.setBounds(2, 2, 970, 640);
        panelResultados.add(panel);
        panelResultados.repaint();
    }

    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Ruteo">
    

    //Metodo que realiza el re-ruteo de las rutas seleccionadas por las hormigas
    public static EstablisedRoute realizarRuteo(String algoritmo, Demand demanda, GraphMatrix grafoCopia, List<GraphPath<Integer, Link>> ksp, int capacidadE, int core) {
        EstablisedRoute r = null;
        switch (algoritmo) {
            case "FA":
                r = Algoritmos_Defrag_ProAct.Def_FA(grafoCopia, demanda, ksp, capacidadE, core);
                break;
            /*case "FA-CA":
                r = Algoritmos_Defrag_ProAct.Def_FACA(grafoCopia, demanda, ksp, capacidadE);
                break;
            case "MTLSC":
                r = Algoritmos.MTLSC_Algorithm(grafoCopia, demanda, ksp, capacidadE);
                break;*/
        }
        return r;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Desfragmentación">
    

    /**
     * UTILIDADES DE DESFRAGMENTACIÓN
     */
    /**
     * Algoritmo que se encarga de asignar a una demanda los FSs requeridos en
     * la red donde se realiza defragmentaciones- se utiiza en la clase
     * ventanaPricipal_Defrag
     *
     * @param ksp
     * @param r
     * @param G
     * @param d
     * @param conexid
     */
    public static void asignarFS_Defrag(GraphPath<Integer, Link> ksp, EstablisedRoute r, GraphMatrix G, Demand d, int conexid) {
        int util;
        int cont;
        int core = d.getCore();
        for (Link link : ksp.getEdgeList()) {
            cont = 0;
            for (int p = r.getFrom(); cont <= d.getFs()&& p <= r.getTo(); p++) {
                cont++;
                if (!G.acceder(link.getFrom(), p).getCores().get(core).getFs().get(p).isFree()) {
                    System.out.println("HAY UN CONFLICTO AL GUARDAR EL ESTADO DEL FS IDA");
                }
                G.acceder(link.getFrom(), link.getTo()).getCores().get(core).getFs().get(p).setFree(Boolean.FALSE);
                if (!G.acceder(link.getFrom(), link.getTo()).getCores().get(core).getFs().get(p).isFree()) {
                    System.out.println("HAY UN CONFLICTO AL GUARDAR EL ESTADO DEL FS VUELTA");
                }
                G.acceder(link.getTo(), link.getFrom()).getCores().get(core).getFs().get(p).setFree(Boolean.FALSE);
                // ¿?
                //G.acceder(nod.getSiguiente().getDato(), nod.getDato()).getFS()[p].setConexion(-conexid);
                //util = G.acceder(nod.getSiguiente().getDato(), nod.getDato()).getUtilizacion()[p]++;
                //G.acceder(nod.getSiguiente().getDato(), nod.getDato()).setUtilizacionFS(p, util);
            }
        }
    }

    public static double calculoMejoraAG(boolean porMsi, boolean porBfr, GraphMatrix copiaGrafo, double bfrGrafo, int capacidad, double msiGrafo, int core) {
        double resultado = 0.0;
        double msiActual;
        double bfrActual;
        if (porMsi) {
            msiActual = Metricas.MSI(copiaGrafo, capacidad, core);
            resultado = 100 - ((redondearDecimales(msiActual, 6) * 100) / redondearDecimales(msiGrafo, 6));
        }
        return resultado;
    }

    //Metodo que ordena el vector de probabilidades de forma creciente
    //reordenando tambien los vectores de feromonas y visibilidad, y el array de rutas.
    public static void ordenarProbabilidad(double[] probabilidad, ArrayList<Integer> orden) {
        double auxp;
        int auxi;
        int n = probabilidad.length;
        for (int i = 0; i <= n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (probabilidad[i] > probabilidad[j]) {
                    auxp = probabilidad[i];
                    probabilidad[i] = probabilidad[j];
                    probabilidad[j] = auxp;

                    //cambiar el orden del vector de indices
                    auxi = orden.get(i);
                    orden.set(i, orden.get(j));
                    orden.set(j, auxi);
                }
            }
        }
    }

    /*
     Comprueba si un elemento ya esta en la lista
    @param
        lista - lista a revisar
        elem - elemnto  a comprobar
    @return
        true si esta, false otherwise
     */
    public static boolean isInList(ArrayList<Integer> lista, int elem) {
        boolean repetido = false;
        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i).equals(elem)) {
                repetido = true;
            }
        }
        return repetido;
    }

    //Metodo que elige la ruta a seleccionar de acuerdo a su vector de probabilidades
    public static int elegirRuta(double[] p, ArrayList<Integer> indices, ArrayList<Integer> indexOrden) {
        //System.out.println("Inicia metodo de Ruleta ");
        int indice;
        int ultPosProbCero = -1; //variable para saber la ultima pos cuando hayan con prob cero y retornar uno de esos, -1 nunca debería enviar
        ArrayList<Integer> indicesProbab = new ArrayList<>();
        for (int i = 0; i < indices.size(); i++) {
            indicesProbab.add(indexOrden.indexOf(indices.get(i)));
        }
        //sumar todas las probabilidades que siguen en juego
        double sumaProbParticipan = 0;
        int n = p.length;
        for (int i = 0; i <= n - 1; i++) {
            if (!isInList(indicesProbab, i)) {
                sumaProbParticipan = sumaProbParticipan + p[i];
                if (p[i] == 0) {
                    ultPosProbCero = i;
                }
            }
        }

        //si ya solo quedan opcines con prob cero, le envio el ultimo que no se eligió aún con prob cero
        if (sumaProbParticipan == 0) {
            return ultPosProbCero;
        }

        //hallar el valor random entre 0 a el valor máximo de probabilidades en juego
        Random randomGenerator = new Random();
        double randomValue = sumaProbParticipan * randomGenerator.nextDouble();

        double sumaProb = 0;
        indice = -1;
        while (sumaProb <= randomValue) {
            indice++;
            if (indice >= p.length) {
                System.out.println();
            }
            if (!isInList(indicesProbab, indice)) {
                sumaProb = sumaProb + p[indice];
            }
        }

        if (indice >= p.length || indice < 0) {
            System.out.println("oh ooh, mando índice: " + indice + ", máximo: " + p.length);
        }
        return indice;
    }

    //Metodo para realizar la copia de un grafo item por item
    public static void copiarGrafo(GraphMatrix copia, GraphMatrix original, int capacidad) {
        copia = original;
    }

    /*Metodo que se encarga de desasignar los FS de una ruta marcada para reconfiguracion en el grafo matriz copia*/
    public static void desasignarFS_DefragProAct(List<GraphPath<Integer, Link>> rutas, ArrayList<EstablisedRoute> r, GraphMatrix G, ArrayList<Integer> indices, int core) {
        for (int i = 0; i < rutas.size(); i++) {
            for (Link nod : rutas.get(i).getEdgeList()) {
                for (int p = r.get(indices.get(i)).getFrom(); p <= r.get(indices.get(i)).getTo(); p++) {
                    if (G.acceder(nod.getFrom(), nod.getTo()).getCores().get(core).getFs().get(p).isFree()) {
                        System.out.println("CONFLICTO AL DESASIGNAR UN SLOT ida. (NO ESTA LUEGO ASIGNADO). Nodo: " + nod.getFrom() + ", Posición: " + p);
                    }
                    G.acceder(nod.getFrom(), nod.getTo()).getCores().get(core).getFs().get(p).setFree(Boolean.TRUE);
                    //G.acceder(nod.getDato(), nod.getSiguiente().getDato()).getFS()[p].setConexion(1);
                    //G.acceder(nod.getDato(), nod.getSiguiente().getDato()).setUtilizacionFS(p, 0);
                    if (G.acceder(nod.getTo(), nod.getFrom()).getCores().get(core).getFs().get(p).isFree()) {
                        System.out.println("CONFLICTO AL DESASIGNAR UN SLOT vuelta. (NO ESTA LUEGO ASIGNADO). Nodo: " + nod.getFrom()+ ", Posición: " + p);
                    }
                    G.acceder(nod.getTo(), nod.getFrom()).getCores().get(core).getFs().get(p).setFree(Boolean.TRUE);
                    //G.acceder(nod.getSiguiente().getDato(), nod.getDato()).getFS()[p].setConexion(-1);
                    //G.acceder(nod.getSiguiente().getDato(), nod.getDato()).setUtilizacionFS(p, 0);
                }
            }
        }
    }
    //</editor-fold>
    
    public static int executionPercentage(int execDemands,int totalDemands){
        int percentage;
        percentage = Math.round(100 * execDemands / totalDemands) ;
        return percentage;
    }

}
