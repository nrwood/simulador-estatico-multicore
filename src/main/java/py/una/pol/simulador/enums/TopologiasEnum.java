package py.una.pol.simulador.enums;

/**
 *
 * @author Néstor E. Reinoso Wood
 */
public enum TopologiasEnum {
    NSFNET(14, "nsfnet.json"),
    USNET(24, "usnet.json"),
    EUNET(15, "eunet.json"),
    NETWORKS(6, "networks.json");

    private TopologiasEnum(Integer size, String file) {
        this.size = size;
        this.file = file;
    }

    private Integer size;

    private String file;

    public Integer size() {
        return size;
    }

    public String file() {
        return file;
    }
}
